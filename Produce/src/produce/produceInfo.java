/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package produce;

/**
 *
 * @author ketay
 */
public class produceInfo {
    
    String name;
    double price;
    
    
    public produceInfo(String myName){
        name = myName;
    } 
    public produceInfo(String name, double price){
        this.name = name;
        this.price = price;
    }
    public String getName(){
        return name;
    }
    public double getPrice(){
        return price;
    }
    public String toString(){
        return("\nProduce: " + this.name + "\nPrice: " + this.price + "$" + "\n--------------");
    }
}
