/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BookDetail;

/**
 *
 * @author ketay
 */
public class BookDetail {
     String title;
    String author;
    int isbn;
    int year; 
    String publisher = "Vanier"; 
    public static int amountOfBooks = 0;
    
    
    public BookDetail(String myTitle){
        title = myTitle;
        amountOfBooks++;
    } 
    
    public BookDetail(String title, String author){
        this.title = title;
        this.author = author;
         amountOfBooks++;
    }
    public BookDetail(String title, String author, int isbn){
        this.title = title;
        this.author = author;
        this.isbn = isbn;
         amountOfBooks++;
    } 
    public BookDetail(String title, String author, int isbn, int year){
        this.title = title;
        this.author = author;
        this.isbn = isbn;
        this.year = year;
         amountOfBooks++;
    } 
    public String getTitle(){
    amountOfBooks++;
        return title;
    
    }
    public String getAuthor(){
        return author;
    }
    public int getISBN(){
        return isbn;    
    }
    public int getYear(){
        return year;
    }
    public int getAmountOfBooks(){
        return amountOfBooks;
    }
    public String toString(){
        return("\nBook title:" + this.title +" \nAuthor: " + this.author + " \nYear printed: " + this.year + " \nCode: " + this.isbn +" \n"+ publisher + "\n\n---------------------------");
    }
    public boolean equals(BookDetail other){
        return this.isbn == other.isbn;
    
}
}