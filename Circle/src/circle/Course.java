/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package circle;

/**
 *
 * @author ketay
 */
public class Course {
     String name;
    double grade;
    
    
    public Course(String myName){
        name = myName;
    } 
    public Course(String name, double grade){
        this.name = name;
        this.grade = grade;
    }
    public String getName(){
        return name;
    }
    public double getgrade(){
        return grade;
    }
    public String toString(){
        return("\nClass: " + this.name + "\nGrade: " + this.grade+ "%" + "\n--------------");
    }
}
